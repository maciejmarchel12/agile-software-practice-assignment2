# Assignment 2 - Agile Software Practice. 

Name: Maciej Marchel

## API endpoints.

### Movie Endpoints
- GET api/movies/ - get movies.
- GET api/movies/details - gets movie details from page 1 and 10 movies only
- GET api/movies/tmdb/upcoming - gets the upcoming movies
- GET api/movies/tmdb/genre - gets movie genres
- GET api/movies/tmdb/popular - gets the most popular movies right now
- GET api/movies/tmdb/now_playing - gets the now playing movies
- GET api/movies/tmdb/top_rated - gets the currently top rated movies
- GET api/movies/:id - gets a movie based on id
- GET api/movies/tmdb/:id/images - gets the image of a movie based on movie id
- GET api/movies/tmdb/:id/recommendations - gets the recommended movies based on the movie id
- GET api/movies/tmdb/:id/similar - gets the similar movies based on the movie id
- GET api/movies/tmdb/:id/reviews - gets movie reviews based on the movie id
- POST api/movies/:id/reviews - Add a review for a movie

### User Endpoints
- GET /api/users - gets users
- POST /api/users?action=action - Register or authenticate a user - set action to register or login
- PUT /api/users/:id - Updates users details baseed on user id

## Automated Testing.

```
  Users endpoint
    GET /api/users
database connected to movies on ac-ivftdvo-shard-00-02.kfymony.mongodb.net
      √ should return the 2 users and a status 200
    POST /api/users
      For a register action
        when the payload is correct
          √ should return a success true and a confirmation message (101ms)
      For an authenticate action
        when the payload is correct
          √ should return a 200 status and a generated token (90ms)

  Movies API
    GET /api/movies
      √ should return 20 movies and a status 200 (44ms)
    GET /api/movies/:id
      when the id is valid
        √ should return the matching movie (38ms)
      when the id is invalid
        √ should return the NOT found message
    GET /tmdb/movie/:id/recommendations
No recommendations found for the specified movie ID.
      √ should return movie recommendations for the specified movie ID
    GET /tmdb/movie/:id/images
No images found for the specified movie ID.
      √ should return movie images for the specified movie ID
    GET /tmdb/movie/:id/similar
Unexpected response format. Expected an array of similar movies.
      √ should return similar movies for the specified movie ID
    GET /tmdb/upcoming
Response Body: {}
      √ should return upcoming movies and a status 200
    GET /tmdb/genre
Response Body: {}
      √ should return movie genres and a status 200
    GET /tmdb/popular
Response Body: {}
      √ should return popular movies and a status 200
    GET /tmdb/now_playing
Response Body: {}
      √ should return now playing movies and a status 200
    GET /tmdb/top_rated
Response Body: {}
      √ should return top rated movies and a status 200


  14 passing (3s)
```

## Deployments

### Staging
https://agile-assignment2-staging-mm-779f9a0c791f.herokuapp.com/api/movies

### Production
https://agile-assignment2-prod-mm-b2eca27fe27a.herokuapp.com/api/movies

