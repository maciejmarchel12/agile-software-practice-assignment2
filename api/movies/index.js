import movieModel from './movieModel';
import reviewModel from './reviewModel';
const mongoose = require('mongoose');
import asyncHandler from 'express-async-handler';
import express from 'express';
import { movieReviews } from '../../initialise-dev/reviews'
import {
    getUpcoming,
    getGenres,
    getMovies,
    getTrending,
    getNowPlaying,
    getTopRated,
    getMovie,
    getMovieImages,
    getSimilar,
    getMovieReviews,
    getRecommendations
  } from '../tmdb-api';

const router = express.Router();

router.get('/', asyncHandler(async (req, res) => {
    const movies = await movieModel.find();
    res.status(200).json(movies);
}));

// Get movie details
router.get('/movies/details', asyncHandler(async (req, res) => {
    let { page = 1, limit = 10 } = req.query; // destructure page and limit and set default values
    [page, limit] = [+page, +limit]; //trick to convert to numeric (req.query will contain string values)

    // Parallel execution of counting movies and getting movies using movieModel
    const [total_results, results] = await Promise.all([
        movieModel.estimatedDocumentCount(),
        movieModel.find().limit(limit).skip((page - 1) * limit)
    ]);
    const total_pages = Math.ceil(total_results / limit); //Calculate total number of pages (= total No Docs/Number of docs per page) 

    //construct return Object and insert into response object
    const returnObject = {
        page,
        total_pages,
        total_results,
        results
    };
    res.status(200).json(returnObject);
}));

router.get('/tmdb/upcoming', asyncHandler(async (req, res) => {
    const upcomingMovies = await getUpcoming();
    res.status(200).json(upcomingMovies);
}));

router.get('/tmdb/genre', asyncHandler(async (req, res) => {
    const genres = await getGenres();
    res.status(200).json(genres);
}));

router.get('/tmdb/movie', asyncHandler(async (req, res) => {
    const movies = await getMovies();
    res.status(200).json(movies);
}));

router.get('/tmdb/popular', asyncHandler(async (req, res) => {
    const trending = await getTrending();
    res.status(200).json(trending);
}));

router.get('/tmdb/now_playing', asyncHandler(async (req, res) => {
    const nowPlaying = await getNowPlaying();
    res.status(200).json(nowPlaying);
}));

router.get('/tmdb/top_rated', asyncHandler(async (req, res) => {
    const topRated = await getTopRated();
    res.status(200).json(topRated);
}));

router.get('/:id', asyncHandler(async (req, res) => {
    const { id } = req.params;
    const movie = await movieModel.findByMovieDBId(id);
    if (movie) {
        res.status(200).json(movie);
    } else {
        res.status(404).json({message: 'The resource you requested could not be found.', status_code: 404});
    }
}));

router.get('/tmdb/movie/:id/images', asyncHandler(async (req, res) => {
    const { id } = req.params;
    const movieImages = await getMovieImages(id);
    res.status(200).json(movieImages);
}));

router.get('/tmdb/movie/:id/similar', asyncHandler(async (req, res) => {
    const { id } = req.params;
    const similar = await getSimilar(id);
    res.status(200).json(similar);
}));

router.get('/:id/reviews', (req, res) => {
    const id = parseInt(req.params.id);
    // find reviews in list
    if (movieReviews.id == id) {
        res.status(200).json(movieReviews);
    } else {
        res.status(404).json({
            message: 'The resource you requested could not be found.',
            status_code: 404
        });
    }
  });

//router.get('/tmdb/movie/:id/reviews', asyncHandler(async (req, res) => {
//    const { id } = req.params;
//    const movieReviews = await getMovieReviews(id);
//    res.status(200).json(movieReviews);
//}));

router.get('/tmdb/movie/:id/recommendations', asyncHandler(async (req, res) => {
    const { id } = req.params;
    const recommendations = await getRecommendations(id);
    res.status(200).json(recommendations);
}));

// New route for adding movie reviews

router.post('/:id/reviews', (req, res) => {
    const id = parseInt(req.params.id);
    const { author, content, rating } = req.body;
  
    if (!author || !content) {
      // Handle validation errors
      return res.status(422).json({
        message: 'Author and content are required fields.',
        status_code: 422,
      });
    }
  
    const newReview = {
      movieId: id,
      author: author,
      content: content,
      rating: rating,
      created_at: new Date(),
      updated_at: new Date(),
    };
  
    // Assuming you have a list of reviews named 'reviews'
    reviews.push(newReview);
  
    res.status(201).json(newReview);
  });
  

/* router.post('/:id/reviews', (req, res) => {
    const id = parseInt(req.params.id);
    const { author, content, rating } = req.body;
  
    const movieReviews = {
      author: author,
      content: content,
      rating: rating,
    };
  
    console.log(req.body);
  
    if (movieReviews.id == id) {
      movieReviews.created_at = new Date();
      movieReviews.updated_at = new Date();
      movieReviews.id = uniqid();
      movieReviews.results.push(movieReviews); // push the new review onto the list
      res.status(201).json(movieReviews);
    } else {
      res.status(404).json({
        message: 'The resource you requested could not be found.',
        status_code: 404,
      });
    }
  }); */

/* router.post('/:id/reviews', asyncHandler(async (req, res) => {
    const { id } = req.params;
    const { author, content, rating } = req.body;

    try {
        // Convert the provided id to a valid ObjectId
        const movieId = mongoose.Types.ObjectId(id);

        // Adjust the query to use the converted ObjectId
        const movie = await movieModel.findOne({ _id: movieId });

        // Add log to check if the movie is found
        console.log('Found Movie:', movie);

        if (!movie) {
            res.status(404).json({ message: 'Movie not found' });
            return;
        }

        // Create a new review object
        const newReview = {
            author: author,
            content: content,
            rating: rating,
        };

        // Add the review to the movie's reviews array
        movie.reviews.push(newReview);

        // Save the updated movie object
        await movie.save();

        console.log('Review saved successfully:', newReview);

        res.status(201).json(newReview);
    } catch (error) {
        console.error('Error saving review:', error);
        res.status(500).json({ message: 'Internal Server Error' });
    }
})); */

  
  // Get reviews for a movie
/*   router.get('/:id/reviews', asyncHandler(async (req, res) => {
    const { id } = req.params;
  
    const reviews = await ReviewModel.findByMovieId(id);
  
    res.status(200).json(reviews);
  })); */

export default router;