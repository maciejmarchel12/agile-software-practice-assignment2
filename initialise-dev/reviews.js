const movieReviews = [
    {
        "movieId": 787699,
        "author": "jane doe",
        "content": "this movie is amazing!!",
        "rating": 5
    },
    {
        "movieId": 787699,
        "author": "james smith",
        "content": "this movie is great!!",
        "rating": 4
    },
    {
        "movieId": 787699,
        "author": "movie reviewer",
        "content": "this movie is a fantastic rendition of willy wonka!",
        "rating": 5
    }
]